using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBreaker : MonoBehaviour
{
    private LineRenderer lineRend;
    private Vector3 startMousePos;
    Vector3 worldPosition;

    [SerializeField] private GameObject cube;
    [SerializeField] private Raycast raycast;

    private GameObject a;
    private GameObject b;

    private bool _isOk;
    private void Start()
    {
        lineRend = GetComponent<LineRenderer>();
        lineRend.positionCount = 2;

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.CompareTag("Interactable")) return;
                a  = Instantiate(cube);
                startMousePos = hit.point;
                a.transform.position = startMousePos;
                _isOk = true;
            }
        }

        if (Input.GetMouseButton(0) && _isOk)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitData;
            if (Physics.Raycast(ray, out hitData, 1000))
            {
                worldPosition = hitData.point;
            }
            lineRend.SetPosition(0, new Vector3(startMousePos.x, 0f, startMousePos.z));
            lineRend.SetPosition(1, new Vector3(worldPosition.x, 0f, worldPosition.z));
        }
        if (Input.GetMouseButtonUp(0) && _isOk)
        {
            b = Instantiate(cube);
            b.transform.position = worldPosition;
            raycast.SetObjects(a, b);
            lineRend.SetPosition(0, new Vector3(0, 0f, 0));
            lineRend.SetPosition(1, new Vector3(0, 0f, 0));
            _isOk = false;
        }
    }

}