using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    public GameObject lastHit;
    public Vector3 collision = Vector3.zero;
    public LayerMask layer;

    public GameObject a;
    public GameObject b;

    public bool _isStart = false;

    private List<Collider> _colliedesOnOff;

    private void Start()
    {
        _colliedesOnOff = new List<Collider>();
    }
    private void Update()
    {
        if (!_isStart) return; 
        RaycastHit hit;
        Vector3 fromPosition = a.transform.position;
        Vector3 toPosition = b.transform.position;
        Vector3 direction = toPosition - fromPosition;
        if (Physics.Raycast(a.transform.position, direction, out hit, 100))
        {
            if (hit.collider.gameObject.CompareTag("Road"))
            {
                Road road = hit.collider.gameObject.GetComponent<Road>();
                if (road.IsActive)
                {
                    road.IsActive = false;
                    if (road.IsChanged) {
                        if (road.PossibleConnects[1].FirstObject.QueueOfRoads.Count > 0)
                        {
                            road.PossibleConnects[1].FirstObject.QueueOfRoads[0].SetActive(false);
                            road.PossibleConnects[1].FirstObject.QueueOfRoads.RemoveAt(0);
                        }
                    } else
                    {
                        if (road.PossibleConnects[0].FirstObject.QueueOfRoads.Count > 0)
                        {
                            road.PossibleConnects[0].FirstObject.QueueOfRoads[0].SetActive(false);
                            road.PossibleConnects[0].FirstObject.QueueOfRoads.RemoveAt(0);
                        }
                    }
                }
            }
        }
        _isStart = false;
        Destroy(a);
        Destroy(b);
    }

/*    private void ExampleFunction()
    {

        RaycastHit hit;
        Vector3 fromPosition = a.transform.position;
        Vector3 toPosition = b.transform.position;
        Vector3 direction = toPosition - fromPosition;
        if (Physics.Raycast(a.transform.position, direction, out hit, 100))
        {
            if (hit.collider.gameObject.name == "Road")
            {
                hit.collider.gameObject.SetActive(false);
                ExampleFunction();
            } else if (hit.collider.gameObject.CompareTag("Interactable"))
            {
                hit.collider.enabled = false;
                _colliedesOnOff.Add(hit.collider);
                ExampleFunction();
            }
        }
        
        foreach (var item in _colliedesOnOff)
        {
            item.enabled = true;
        }
        _colliedesOnOff = new List<Collider>();
    }*/

    public void SetObjects(GameObject a1, GameObject b1)
    {
        a = a1;
        b = b1;
        _isStart = true;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(collision, 0.2f);
    }
}
