using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    public static MouseManager Instance;
    [SerializeField] private GameObject _firstObject;
    [SerializeField] private GameObject _secondObject;
    [SerializeField] private Line _lineComponent;

    public GameObject FirstObject { get => _firstObject; set => _firstObject = value; }
    public GameObject SecondObject { get => _secondObject; set => _secondObject = value; }

    private void Start()
    {
        Instance = this;
    }

    public void GiveObject(GameObject obj)
    {
        if (_firstObject == null)
        {
            _firstObject = obj;
        }
        else if (_firstObject == obj || _secondObject == obj)
        {
            _firstObject = null;
            _secondObject = null;
            //_lineComponent.ResetLine();
        }
        else
        {
            _secondObject = obj;
            //_lineComponent.SetPosition(_firstObject.transform.GetChild(0), _secondObject.transform.GetChild(0));

            _firstObject.GetComponent<UnitHolder>().StartUnitMoving(_secondObject.transform);
        }
    }
}
