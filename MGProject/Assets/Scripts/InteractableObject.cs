using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    [SerializeField] private int _id;
    [SerializeField] private Connections _connections;
    [SerializeField] private List<GameObject> _queueOfRoads;
    [SerializeField] private int _connectionNumber;
    public int Id { get => _id; set => _id = value; }
    public Connections Connections { get => _connections; set => _connections = value; }
    public List<GameObject> QueueOfRoads { get => _queueOfRoads; set => _queueOfRoads = value; }

    private void Start()
    {
        _queueOfRoads = new List<GameObject>();
    }
    public void Check(GameObject secondObject)
    {
        print(gameObject.name + " -> " + secondObject.name);
        InteractableObject secIO = secondObject.GetComponent<InteractableObject>();
        foreach (var item in _connections.Roads)
        {
            if (item.PossibleConnects[0].FirstObject.Id == _id && item.PossibleConnects[0].SecondObject.Id == secIO.Id)
            {
                item.IsActive = true;
                if (item.IsChanged)
                {
                    item.gameObject.transform.Rotate(new Vector3(0, 180, 0));
                    item.IsChanged = false;
                }
                item.gameObject.SetActive(true);

                if (_queueOfRoads.Count > _connectionNumber - 1)
                {
                    _queueOfRoads[0].SetActive(false);
                    _queueOfRoads[0].GetComponent<Road>().IsActive = false;
                    _queueOfRoads.RemoveAt(0);
                    _queueOfRoads.Add(item.gameObject);
                }else
                {
                    _queueOfRoads.Add(item.gameObject);
                }
                return;
            } else if (item.PossibleConnects[1].FirstObject.Id == _id && item.PossibleConnects[1].SecondObject.Id == secIO.Id)
            {
                item.IsActive = true;
                if (!item.IsChanged)
                {
                    item.gameObject.transform.Rotate(new Vector3(0, 180, 0));
                    item.IsChanged = true;
                }
                item.gameObject.SetActive(true);

                if (_queueOfRoads.Count > _connectionNumber - 1)
                {
                    _queueOfRoads[0].SetActive(false);
                    _queueOfRoads[0].GetComponent<Road>().IsActive = false;
                    _queueOfRoads.RemoveAt(0);
                    _queueOfRoads.Add(item.gameObject);
                }
                else
                {
                    _queueOfRoads.Add(item.gameObject);
                }
                return;
            } else
            {
                print("No Connection");
            }
        }
    }
    private bool IsExist(int id)
    {
        return false;
    }
}
