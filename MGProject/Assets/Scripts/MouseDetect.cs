using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDetect : MonoBehaviour
{
    [SerializeField] private Renderer _renderer;
    [SerializeField] private Material _standardMaterial;
    [SerializeField] private Material _hoveredMaterial;
    private void OnMouseEnter()
    {
        _renderer.material.color = _hoveredMaterial.color;
        Line.Instance.SecondPosition(gameObject, true);
    }

    private void OnMouseExit()
    {
        _renderer.material.color = _standardMaterial.color;
        Line.Instance.SecondPosition(null, false);
    }

    private void OnMouseDown()
    {
        MouseManager.Instance.GiveObject(gameObject);
    }
}
