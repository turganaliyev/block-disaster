using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UnitHolder : MonoBehaviour
{
    [SerializeField] private Canvas _canvas;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _power;
    [SerializeField] private GameObject _unit;

    private TMP_Text _powerText;
    private void Start()
    {
        Canvas canvas = Instantiate(_canvas);
        SetParent(canvas.transform, transform);
        _powerText = canvas.transform.GetChild(0).gameObject.GetComponent<TMP_Text>();
        _powerText.text = _power.ToString();
    }
    public void SetParent(Transform child, Transform parent)
    {
        child.localPosition = parent.position + _offset;
        child.localRotation = Quaternion.Euler(90f, 0f, 0f);
    }

    public void StartUnitMoving(Transform toPlace)
    {
        var unit = Instantiate(_unit, transform.position, Quaternion.identity);

    }
}
