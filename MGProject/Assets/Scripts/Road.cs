using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    [SerializeField] private int _id;
    [SerializeField] private bool _isChanged;
    [SerializeField] private bool _isActive;
    [SerializeField] private PossibleConnects[] _possibleConnects;
    public int Id { get => _id; set => _id = value; }
    public PossibleConnects[] PossibleConnects { get => _possibleConnects; set => _possibleConnects = value; }
    public bool IsChanged { get => _isChanged; set => _isChanged = value; }
    public bool IsActive { get => _isActive; set => _isActive = value; }
}
[System.Serializable]
public class PossibleConnects
{
    public InteractableObject FirstObject;
    public InteractableObject SecondObject;
}
