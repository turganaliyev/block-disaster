using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public static Line Instance;
    //[SerializeField] private GameObject _road;

    private LineRenderer lineRend;
    private Vector3 startMousePos;

    private bool _isClicked;
    private bool _isDetected;

    private Vector3 objectPosition;
    private Vector3 worldPosition;

    private GameObject _firstObject;
    private GameObject _secondObject;
    private void Start()
    {
        Instance = this;
        //_road.SetActive(false);
        lineRend = GetComponent<LineRenderer>();
        lineRend.positionCount = 2;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.CompareTag("Interactable"))
                {
                    _isClicked = true;
                    _firstObject = hit.transform.gameObject;
                    startMousePos = hit.transform.position;
                }
            }
        }

        if (Input.GetMouseButton(0) && _isClicked)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitData;
            if (Physics.Raycast(ray, out hitData, 1000))
            {
                worldPosition = hitData.point;
            }
            lineRend.SetPosition(0, new Vector3(startMousePos.x, 0f, startMousePos.z));
            lineRend.SetPosition(1, new Vector3(worldPosition.x, 0f, worldPosition.z));
        }
        if (Input.GetMouseButtonUp(0))
        {
            _isClicked = false;
            lineRend.SetPosition(0, new Vector3(0, 0f, 0));
            lineRend.SetPosition(1, new Vector3(0, 0f, 0));
        }
        if (Input.GetMouseButtonUp(0) && _isDetected && _firstObject != null)
        {
            _isDetected = false;
            _isClicked = false;
            _firstObject.GetComponent<InteractableObject>().Check(_secondObject);
            _firstObject = null;
            _secondObject = null;
            //_road.SetActive(true);
        }
    }

    public void SecondPosition(GameObject detectedObject, bool trigger) {
        _isDetected = trigger;
        _secondObject = detectedObject;
    }
}